export interface Mappable {
  location: {
    lat: number;
    lng: number;
  },
  markerContent(): string;
}

export class CustomMap {
  private googleMap: google.maps.Map;
  async initMap(divId: string): Promise<void> {
    const { Map } = await google.maps.importLibrary("maps") as google.maps.MapsLibrary;

    this.googleMap = new Map(document.getElementById(divId) as HTMLElement, {
      center: { lat: 0, lng: 0 },
      zoom: 1,
      mapId: 'DEMO'
    });
  }

  async addMarker(mappable: Mappable): Promise<void> {
    const { AdvancedMarkerElement } = await google.maps.importLibrary("marker") as google.maps.MarkerLibrary;
    const marker = new AdvancedMarkerElement({
      map: this.googleMap,
      position: mappable.location,
      title: 'Marker'
    });

    marker.addListener('click', () => {
      const infoWindow = new google.maps.InfoWindow({
        content: mappable.markerContent()
      });

      infoWindow.open({
        anchor: marker,
        map: this.googleMap,
      });
    });
  }

  constructor(divId: string) {
    this.initMap(divId).then(() => {
      console.log('map loaded')
    });
  }
}
