import {User} from "./User";
import {Company} from "./Company";
import {CustomMap} from "./CustomMap";

import './style.css';

const user = new User();
const company = new Company()

const customMap = await new CustomMap('map')
customMap.addMarker(user).then(() => console.log('user marker added'))
customMap.addMarker(company).then(() => console.log('company marker added'))
